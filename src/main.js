import {createApp} from 'vue'
import App from './App.vue'
import PrimeVue from "primevue/config";
import "primevue/resources/themes/saga-blue/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import Paginator from 'primevue/paginator';
import MegaMenu from "primevue/megamenu";
let app = createApp(App)

app.use(PrimeVue)
// app.component('Paginator', Paginator)
app.component("MegaMenu", MegaMenu)
app.component('Paginator', Paginator)
app.mount('#app')


